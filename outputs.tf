output "instance_ip_addrs" {
	description = "Primary IP address of each instance"
	value = {
		for k, v in module.debvirt_instance : k => v.instance_ip_primary
	}
}
