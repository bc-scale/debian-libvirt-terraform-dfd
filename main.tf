terraform {
	backend "local" {
		path = "/var/local/terraform-state/dev001/terraform.tfstate"
	}

	required_providers {
		libvirt = {
			source = "dmacvicar/libvirt"
		}
		random = {
			source = "hashicorp/random"
		}
	}
}

provider "libvirt" {
	uri = var.libvirt_uri
}

resource "libvirt_volume" "os_base_volume" {
	source = var.libvirt_os_source_location
	name = "${var.envname}-os-base-volume.qcow2"
	pool = var.libvirt_os_base_pool
}

resource "libvirt_network" "envnet_nat" {
	autostart = true
	addresses = compact([var.envnet_v4cidr, var.envnet_v6cidr])
	domain = coalesce(var.internal_domain, var.envname)
	mode = "nat"
	name = var.envname
	dhcp {
		enabled = true
	}
	dns {
		enabled = true
	}
}

resource "libvirt_volume" "cluster_data_volumes" {
	for_each = var.cluster_data_volumes

	format = each.value.format
	name = each.value.name
	pool = each.value.pool
	size = each.value.size

	lifecycle {
		prevent_destroy = true
	}
}

module "debvirt_instance" {
	for_each = var.instantiation_data
	source = "./modules/instance_common"

	admin_base_homedir = var.admin_base_homedir
	admin_gecos = var.admin_gecos
	admin_sshkey = var.admin_sshkey
	admin_username = var.admin_username
	apt_http_proxy = var.apt_http_proxy
	cloudinit_volume_pool = var.cloudinit_volume_pool
	conn_bastion_host = each.value.conn_bastion_host
	conn_bastion_port = each.value.conn_bastion_port
	conn_bastion_user = each.value.conn_bastion_user
	conn_port = each.value.conn_port
	conn_timeout = each.value.conn_timeout
	conn_user = each.value.conn_user
	data_volume_id = try(libvirt_volume.cluster_data_volumes[each.key].id, "")
	envname = var.envname
	fail2ban_ignore_ip = each.value.fail2ban_ignore_ip
	id_base = each.value.id_base
	internal_domain = var.internal_domain
	network_id = libvirt_network.envnet_nat.id
	ntp_servers = var.ntp_servers
	os_base_volume_id = libvirt_volume.os_base_volume.id
	os_volume_pool = each.value.os_volume_pool
	os_volume_size = each.value.os_volume_size
	userdata_apt = each.value.userdata_apt
	userdata_bootcmds = each.value.userdata_bootcmds
	userdata_disk_setups = each.value.userdata_disk_setups
	userdata_fs_setups = each.value.userdata_fs_setups
	userdata_groups = each.value.userdata_groups
	userdata_mounts = each.value.userdata_mounts
	userdata_packages = each.value.userdata_packages
	userdata_runcmds = each.value.userdata_runcmds
	userdata_ssh_genkeytypes = each.value.userdata_ssh_genkeytypes
	userdata_ssh_keys = each.value.userdata_ssh_keys
	userdata_ufwrules = each.value.userdata_ufwrules
	userdata_users = each.value.userdata_users
	userdata_write_files = each.value.userdata_write_files
	userdata_write_templates = each.value.userdata_write_templates
	vm_memory = each.value.vm_memory
	vm_name = each.value.vm_name
	vm_vcpu = each.value.vm_vcpu

	depends_on = [
		libvirt_network.envnet_nat
	]
}
