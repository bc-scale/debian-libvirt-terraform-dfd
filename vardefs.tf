# Provider configuration

variable "libvirt_uri" {
	default = "qemu:///system"
	description = "Libvirt connection URI (e.g. `qemu+ssh://user@remote-host/system`)"
	type = string
}

## Common variables
variable "admin_base_homedir" {
	default = "/local-home"
	description = "With homedir_base that is not /home allows /home of most users to be on a separate volume (and not destroyed on VM rebuild)."
	type = string
}

variable "admin_gecos" {
	default = ""
	description = "'Friendly' name for administrative user of instance (e.g. 'Debian Admin')"
	sensitive = true
	type = string
}

variable "admin_sshkey" {
	description = "SSH publickey for administrative user (private key must be added to 'ssh-agent' in Terraform's environment)"
	sensitive = true
	type = string
}

variable "admin_username" {
	default = "admin"
	description = "Username for administrative user (can read most logs without sudo, has sudo capability)"
	type = string
}

# To avoid using excessive time and bandwidth having a local cache
# or mirror of the package repos is recommended. apt-cacher-ng is
# convenient in that you don't need to download a full mirror, instead
# you are able to reuse the packages downloaded by other instances.
variable "apt_http_proxy" {
	default = ""
	description = "HTTP proxy for apt"
	type = string
}

variable "cloudinit_volume_pool" {
	default = "default"
	description = "Libvirt storage pool for every 'NoCloud' (CIDATA) ISO for cloud-init userdata for instances in the cluster"
	type = string
}

variable "cluster_data_volumes" {
	description = "Map of instances to data volumes for the instance. Not in instance_data to bug in Terraform when using for_each with an attribute inside an attribute."
	type = map(object({
		format = string
		name = string
		pool = string
		size = number
	}))
}

variable "envname" {
	description = "Name of cluster group ('environment', e.g. 'dev001'' for the first development environment)"
	type = string
}

variable "envnet_v4cidr" {
	default = null
	description = "Cluster's internal network v4 CIDR (zero-based). Optional."
	nullable = true
	type = string
}

variable "envnet_v6cidr" {
	default = null
	description = "Cluster's internal network v6 CIDR (zero-based). Optional."
	nullable = true
	type = string
}

variable "instantiation_data" {
	description = "Structure holding configuration values for all instances"
	type = map(object({
		conn_bastion_host = optional(string)
		conn_bastion_port = optional(number)
		conn_bastion_user = optional(string)
		conn_port = number
		conn_timeout = string
		conn_user = string
		fail2ban_ignore_ip = optional(string)
		id_base = optional(string)
		os_volume_pool = optional(string)
		os_volume_size = optional(string)
		userdata_apt = optional(object({
			add_apt_repo_match = optional(string)
			conf = optional(string)
			debconf_selections = optional(map(string))
			disable_suites = optional(list(string))
			ftp_proxy = optional(string)
			http_proxy = optional(string)
			https_proxy = optional(string)
			preserve_sources_list = optional(bool)
			primary = optional(list(object({
				arches = list(string)
				uri = optional(list(string))
				search = optional(list(string))
				search_dns = optional(bool)
			})))
			proxy = optional(string)
			security = optional(list(object({
				arches = list(string)
				uri = optional(list(string))
				search = optional(list(string))
				search_dns = optional(bool)
			})))
			sources = optional(map(object({
				key = optional(string)
				keyid = optional(string)
				keyserver = optional(string)
				source = string
			})))
			sources_list = optional(string)
		}))
		userdata_bootcmds = optional(list(list(string)))
		userdata_disk_setups = optional(map(list(number)))
		userdata_fs_setups = optional(map(list(number)))
		userdata_groups = optional(list(string))
		userdata_mounts = optional(list(list(string)))
		userdata_packages = optional(list(string))
		userdata_runcmds = optional(list(list(string)))
		userdata_ssh_genkeytypes = optional(list(string))
		userdata_ssh_keys = optional(object({
			ecdsa_private = string
			ecdsa_public = string
			ed25519_private = string
			ed25519_public = string
		}))
		userdata_ufwrules = optional(list(object({
			from = string
			interface = string
			port = number
			proto = optional(string)
			to = string
		})))
		userdata_users = optional(list(object({
			name = string
			gecos = optional(string)
			groups = optional(list(string))
			homedir = optional(string)
			lock_passwd = optional(bool)
			shell = optional(string)
			ssh_authorized_keys = optional(list(string))
			system = optional(bool)
		})))
		userdata_write_files = optional(list(object({
			append = optional(bool)
			encoding = optional(string)
			file_source_path = string
			owner = optional(string)
			path = string
			permissions = optional(string)
		})))
		userdata_write_templates = optional(list(object({
			append = optional(bool)
			encoding = optional(string)
			owner = optional(string)
			path = string
			permissions = optional(string)
			template_source_path = string
			tvars = any
		})))
		vm_memory = number
		vm_name = string
		vm_vcpu = number
	}))
}

variable "internal_domain" {
	description = "Cluster's internal network domain name"
	nullable = true
	type = string
}

variable "libvirt_os_base_pool" {
	description = "Libvirt storage pool for the base OS volume used for every instance in the cluster"
	type = string
}

variable "libvirt_os_source_location" {
	description = "Source URI or local path for the base OS volume used for every instance in the cluster"
	type = string
}

variable "ntp_servers" {
	default = null
	description = "If specified all VMs will use these servers for NTP instead of the default. Forces use of chrony"
	nullable = true
	type = list(string)
}
