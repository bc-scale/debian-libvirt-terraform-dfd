# DFD Debian VM cluster using Terraform on Libvirt

Daniel F. Dickinson's Libvirt virtual machine deployment for a 'personal
infrastructure' cluster using Terraform. This brings up the cluster in a safe
state, but expects further customisation (for example an Ansible run).

This repository should not include any private or even particularly personalised
information. It does, however, depend on adding some (documented) variables and
supplying them to the Terraform plan command (e.g. using
`-var-file my-personalisation.tfvars.hcl`).

Despite not having personalised details, it is still designed for DFD's uses and
is likely not a 'plug and play' option for others. It is Daniel's hope, however,
that it still proves informative and useful.

## Metadata

### Demo and/or documentation site or page

Not yet created.

### Repository URL

<https://gitlab.com/danielfdickinson/debian-libvirt-terraform-dfd>

## Features

TBD

## Settings

[//]: # (BEGIN_TF_DOCS)

### Requirements

No requirements.

### Providers

| Name | Version |
|------|---------|
| libvirt | 0.7.0 |

### Modules

| Name | Source | Version |
|------|--------|---------|
| debvirt\_instance | ./modules/instance_common | n/a |

### Resources

| Name | Type |
|------|------|
| [libvirt_network.envnet_nat](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/network) | resource |
| [libvirt_volume.cluster_data_volumes](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/volume) | resource |
| [libvirt_volume.os_base_volume](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/volume) | resource |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| admin\_base\_homedir | With homedir\_base that is not /home allows /home of most users to be on a separate volume (and not destroyed on VM rebuild). | `string` | `"/local-home"` | no |
| admin\_gecos | 'Friendly' name for administrative user of instance (e.g. 'Debian Admin') | `string` | `""` | no |
| admin\_sshkey | SSH publickey for administrative user (private key must be added to 'ssh-agent' in Terraform's environment) | `string` | n/a | yes |
| admin\_username | Username for administrative user (can read most logs without sudo, has sudo capability) | `string` | `"admin"` | no |
| apt\_http\_proxy | HTTP proxy for apt | `string` | `""` | no |
| cloudinit\_volume\_pool | Libvirt storage pool for every 'NoCloud' (CIDATA) ISO for cloud-init userdata for instances in the cluster | `string` | `"default"` | no |
| cluster\_data\_volumes | Map of instances to data volumes for the instance. Not in instance\_data to bug in Terraform when using for\_each with an attribute inside an attribute. | <pre>map(object({<br>		format = string<br>		name = string<br>		pool = string<br>		size = number<br>	}))</pre> | n/a | yes |
| envname | Name of cluster group ('environment', e.g. 'dev001'' for the first development environment) | `string` | n/a | yes |
| envnet\_v4cidr | Cluster's internal network v4 CIDR (zero-based). Optional. | `string` | `null` | no |
| envnet\_v6cidr | Cluster's internal network v6 CIDR (zero-based). Optional. | `string` | `null` | no |
| instantiation\_data | Structure holding configuration values for all instances | <pre>map(object({<br>		conn_bastion_host = optional(string)<br>		conn_bastion_port = optional(number)<br>		conn_bastion_user = optional(string)<br>		conn_port = number<br>		conn_timeout = string<br>		conn_user = string<br>		fail2ban_ignore_ip = optional(string)<br>		id_base = optional(string)<br>		os_volume_pool = optional(string)<br>		os_volume_size = optional(string)<br>		userdata_apt = optional(object({<br>			add_apt_repo_match = optional(string)<br>			conf = optional(string)<br>			debconf_selections = optional(map(string))<br>			disable_suites = optional(list(string))<br>			ftp_proxy = optional(string)<br>			http_proxy = optional(string)<br>			https_proxy = optional(string)<br>			preserve_sources_list = optional(bool)<br>			primary = optional(list(object({<br>				arches = list(string)<br>				uri = optional(list(string))<br>				search = optional(list(string))<br>				search_dns = optional(bool)<br>			})))<br>			proxy = optional(string)<br>			security = optional(list(object({<br>				arches = list(string)<br>				uri = optional(list(string))<br>				search = optional(list(string))<br>				search_dns = optional(bool)<br>			})))<br>			sources = optional(map(object({<br>				key = optional(string)<br>				keyid = optional(string)<br>				keyserver = optional(string)<br>				source = string<br>			})))<br>			sources_list = optional(string)<br>		}))<br>		userdata_bootcmds = optional(list(list(string)))<br>		userdata_disk_setups = optional(map(list(number)))<br>		userdata_fs_setups = optional(map(list(number)))<br>		userdata_groups = optional(list(string))<br>		userdata_mounts = optional(list(list(string)))<br>		userdata_packages = optional(list(string))<br>		userdata_runcmds = optional(list(list(string)))<br>		userdata_ssh_genkeytypes = optional(list(string))<br>		userdata_ssh_keys = optional(object({<br>			ecdsa_private = string<br>			ecdsa_public = string<br>			ed25519_private = string<br>			ed25519_public = string<br>		}))<br>		userdata_ufwrules = optional(list(object({<br>			from = string<br>			interface = string<br>			port = number<br>			proto = optional(string)<br>			to = string<br>		})))<br>		userdata_users = optional(list(object({<br>			name = string<br>			gecos = optional(string)<br>			groups = optional(list(string))<br>			homedir = optional(string)<br>			lock_passwd = optional(bool)<br>			shell = optional(string)<br>			ssh_authorized_keys = optional(list(string))<br>			system = optional(bool)<br>		})))<br>		userdata_write_files = optional(list(object({<br>			append = optional(bool)<br>			encoding = optional(string)<br>			file_source_path = string<br>			owner = optional(string)<br>			path = string<br>			permissions = optional(string)<br>		})))<br>		userdata_write_templates = optional(list(object({<br>			append = optional(bool)<br>			encoding = optional(string)<br>			owner = optional(string)<br>			path = string<br>			permissions = optional(string)<br>			template_source_path = string<br>			tvars = any<br>		})))<br>		vm_memory = number<br>		vm_name = string<br>		vm_vcpu = number<br>	}))</pre> | n/a | yes |
| internal\_domain | Cluster's internal network domain name | `string` | n/a | yes |
| libvirt\_os\_base\_pool | Libvirt storage pool for the base OS volume used for every instance in the cluster | `string` | n/a | yes |
| libvirt\_os\_source\_location | Source URI or local path for the base OS volume used for every instance in the cluster | `string` | n/a | yes |
| libvirt\_uri | Libvirt connection URI (e.g. `qemu+ssh://user@remote-host/system`) | `string` | `"qemu:///system"` | no |
| ntp\_servers | If specified all VMs will use these servers for NTP instead of the default. Forces use of chrony | `list(string)` | `null` | no |

### Outputs

| Name | Description |
|------|-------------|
| instance\_ip\_addrs | Primary IP address of each instance |

[//]: # (END_TF_DOCS)

### See also the 'instance_common' module settings

[Common module settings README](modules/instance_common/README.md)

## Using this cluster deployment

1. Clone the repository and cd into it

	```bash
	git clone https://gitlab.com/danielfdickinson/debian-libvirt-terraform-dfd
	cd debian-libvirt-terraform-dfd
	```

2. The main steps: TBD.

## Getting help, discussing, and/or modifying

TBD

-------

## Colophon

* [Copyright and licensing](LICENSE)
* [Inspirations, information, and source material](ACKNOWLEDGEMENTS.md)
* [Notes](README-NOTES.md)
