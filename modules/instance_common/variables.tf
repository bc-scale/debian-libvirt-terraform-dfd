variable "admin_base_homedir" {
	default = "/local-home"
	description = "With base_homedir that is not /home allows /home of most users to be on a separate volume (and not destroyed on VM rebuild)."
	type = string
}

variable "admin_gecos" {
	default = ""
	description = "'Friendly' name for administrative user of instance (e.g. 'Debian Admin')"
	sensitive = true
	type = string
}

variable "admin_sshkey" {
	description = "SSH publickey for administrative user (private key must be added to 'ssh-agent' in Terraform's environment)"
	sensitive = true
	type = string
}

variable "admin_username" {
	default = "admin"
	description = "Username for administrative user (can read most logs without sudo, has sudo capability)"
	type = string
}

# To avoid using excessive time and bandwidth having a local cache
# or mirror of the package repos is recommended. apt-cacher-ng is
# convenient in that you don't need to download a full mirror, instead
# you are able to reuse the packages downloaded by other instances.
variable "apt_http_proxy" {
	default = ""
	description = "HTTP proxy for apt"
	type = string
}

variable "cloudinit_volume_pool" {
	description = "Libvirt storage pool for 'NoCloud' (CIDATA) ISO for cloud-init userdata"
	type = string
}

variable "conn_bastion_host" {
	default = null
	description = "DNS name or IP address by which Terraform host can access the bastion through which to access the instance. Null for a direct connection to instance"
	nullable = true
	type = string
}

variable "conn_bastion_port" {
	default = null
	description = "SSH port bastion host through which the target instance is reached. Null for a direct connection to the instance"
	nullable = true
	sensitive = true
	type = number
}

variable "conn_bastion_user" {
	default = null
	description = "Username of user on bastion host through which the target instance is reached. Null for a direct connection to the instance"
	nullable = true
	type = string
}

variable "conn_domain" {
	default = null
	description = "DNS domain name for accessing this instance from the bastion host"
	nullable = true
	type = string
}

variable "conn_port" {
	default = null
	description = "SSH port on which to reach this instance"
	nullable = true
	type = number
}

variable "conn_timeout" {
	default = null
	description = "The timeout to wait for the connection to become available. Should be provided as a string (e.g., '30s' or '5m'.)"
	nullable = true
	type = string
}

variable "conn_user" {
	description = "Username of admin user on this instance"
	type = string
}

variable "data_volume_id" {
	description = "Volume ID for data volume for instance, if applicable"
	nullable = true
	type = string
}

variable "envname" {
	description = "Environment (group) name for cluster"
	type = string
}

variable "fail2ban_ignore_ip" {
	default = ""
	description = "Non-localhost ip addresses or CIDRs that fail2ban should ignore"
	sensitive = true
}

variable "id_base" {
	default = ""
	description = "cloud-init instance-id base; empty string (\"\") means recreate os volume, cloudinit, and domain on any userdata change, otherwise a change in this id_base will result in that full change"
	sensitive = true
	type = string
}

variable "internal_domain" {
	description = "Cluster's internal network domain name"
	nullable = true
	type = string
}

variable "network_id" {
	description = "ID of network for attaching the network interface"
	type = string
}

variable "ntp_servers" {
	default = null
	description = "If specified VM will use these servers for NTP instead of the default. Forces use of chrony"
	nullable = true
	type = list(string)
}

variable "os_base_volume_id" {
	description = "volume-id of Libvirt image to use as backing store of OS disk image"
	sensitive = true
	type = string
}

variable "os_volume_pool" {
	default = null
	description = "Libvirt storage pool for the OS disk image"
	nullable = true
	type = string
}

variable "os_volume_size" {
	default = null
	description = "Size of OS disk image in bytes"
	nullable = true
	type = string
}

variable "userdata_apt" {
	default = null
	description = "Apt configuration"
	nullable = true
	type = object({
		add_apt_repo_match = optional(string)
		conf = optional(string)
		debconf_selections = optional(map(string))
		disable_suites = optional(list(string))
		ftp_proxy = optional(string)
		http_proxy = optional(string)
		https_proxy = optional(string)
		preserve_sources_list = optional(bool)
		primary = optional(list(object({
			arches = list(string)
			uri = optional(list(string))
			search = optional(list(string))
			search_dns = optional(bool)
		})))
		proxy = optional(string)
		security = optional(list(object({
			arches = list(string)
			uri = optional(list(string))
			search = optional(list(string))
			search_dns = optional(bool)
		})))
		sources = optional(map(object({
			key = optional(string)
			keyid = optional(string)
			keyserver = optional(string)
			source = string
		})))
		sources_list = optional(string)
	})
}

variable "userdata_bootcmds" {
	default = null
	description = "Additional shell commands to run at the start of 'cloud-init' (certain prerequisite commands are always run, for this repo)"
	nullable = true
	sensitive = true
	type = list(list(string))
}

variable "userdata_disk_setups" {
	default = null
	description = "Map of disks to partitioning schemes"
	nullable = true
	type = map(list(number))
}

variable "userdata_fs_setups" {
	default = null
	description = "Map of disks to list of partitions to format as ext4 if not already formatted"
	nullable = true
	type = map(list(number))
}

variable "userdata_groups" {
	default = null
	description = "List of additional groups to ensure exist on system"
	nullable = true
	type = list(string)
}

variable "userdata_mounts" {
	default = null
	description = "List of fstab entries to add to system"
	nullable = true
	type = list(list(string))
}

variable "userdata_packages" {
	default = null
	description = "List of packages to install via userdata packages config"
	nullable = true
	type= list(string)
}

variable "userdata_runcmds" {
	default = null
	description = "Additional shell commands to run at the end of 'cloud-init' (certain prerequisite commands are always run, for this repo)"
	nullable = true
	sensitive = true
	type = list(list(string))
}

variable "userdata_ssh_genkeytypes" {
	default = null
	description = "SSH host key types to generate"
	nullable = true
	type = list(string)
}

variable "userdata_ssh_keys" {
	default = null
	description = "Specify SSH host keys to use (no new keys will be generated)"
	nullable = true
	sensitive = true
	type = object({
		ecdsa_private = string
		ecdsa_public = string
		ed25519_private = string
		ed25519_public = string
	})
}

variable "userdata_ufwrules" {
	default = null
	description = "List of additional firewall (UFW) rules"
	nullable = true
	sensitive = true
	type = list(object({
		from = string
		interface = string
		port = number
		proto = optional(string)
		to = string
	}))
}

variable "userdata_users" {
	default = null
	description = "Additional users to add via cloud-init"
	nullable = true
	sensitive = true
	type = list(object({
		name = string
		gecos = optional(string)
		groups = optional(list(string))
		homedir = optional(string)
		lock_passwd = optional(bool)
		shell = optional(string)
		ssh_authorized_keys = optional(list(string))
		system = optional(bool)
	}))
}

variable "userdata_write_files" {
	default = null
	description = "Additional files to add via cloud-init"
	nullable = true
	sensitive = true
	type = list(object({
		append = optional(bool)
		encoding = optional(string)
		file_source_path = string
		owner = optional(string)
		path = string
		permissions = optional(string)
	}))
}

variable "userdata_write_templates" {
	default = null
	description = "Additional template files to write via cloud-init"
	nullable = true
	sensitive = true
	type = list(object({
		append = optional(bool)
		encoding = optional(string)
		owner = optional(string)
		path = string
		permissions = optional(string)
		template_source_path = string
		tvars = any
	}))
}

variable "vm_memory" {
	default = null
	description = "Max RAM for the instance"
	nullable = true
	type = number
}

variable "vm_name" {
	description = "Hostname and Libvirt domain name for the instance (just one name)"
	type = string
}

variable "vm_vcpu" {
	default = null
	description = "Number of virtual CPU for the instance"
	nullable = true
	type = number
}

locals {
	conn_domain_separator = var.conn_domain != null ? (var.conn_domain != "" ? "." : "") : ""
}
