# DFD VM cluster instances common configuration module

## Module definition

[//]: # (BEGIN_TF_DOCS)

### Requirements

No requirements.

### Providers

| Name | Version |
|------|---------|
| libvirt | n/a |
| random | n/a |

### Modules

No modules.

### Resources

| Name | Type |
|------|------|
| [libvirt_cloudinit_disk.instance_cloudinit](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/cloudinit_disk) | resource |
| [libvirt_domain.instance](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/domain) | resource |
| [libvirt_volume.instance_os_volume](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/volume) | resource |
| [random_id.instance_cloudinit](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [random_id.instance_metadata](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [random_id.instance_os_volume](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| admin\_base\_homedir | With base\_homedir that is not /home allows /home of most users to be on a separate volume (and not destroyed on VM rebuild). | `string` | `"/local-home"` | no |
| admin\_gecos | 'Friendly' name for administrative user of instance (e.g. 'Debian Admin') | `string` | `""` | no |
| admin\_sshkey | SSH publickey for administrative user (private key must be added to 'ssh-agent' in Terraform's environment) | `string` | n/a | yes |
| admin\_username | Username for administrative user (can read most logs without sudo, has sudo capability) | `string` | `"admin"` | no |
| apt\_http\_proxy | HTTP proxy for apt | `string` | `""` | no |
| cloudinit\_volume\_pool | Libvirt storage pool for 'NoCloud' (CIDATA) ISO for cloud-init userdata | `string` | n/a | yes |
| conn\_bastion\_host | DNS name or IP address by which Terraform host can access the bastion through which to access the instance. Null for a direct connection to instance | `string` | `null` | no |
| conn\_bastion\_port | SSH port bastion host through which the target instance is reached. Null for a direct connection to the instance | `number` | `null` | no |
| conn\_bastion\_user | Username of user on bastion host through which the target instance is reached. Null for a direct connection to the instance | `string` | `null` | no |
| conn\_domain | DNS domain name for accessing this instance from the bastion host | `string` | `null` | no |
| conn\_port | SSH port on which to reach this instance | `number` | `null` | no |
| conn\_timeout | The timeout to wait for the connection to become available. Should be provided as a string (e.g., '30s' or '5m'.) | `string` | `null` | no |
| conn\_user | Username of admin user on this instance | `string` | n/a | yes |
| data\_volume\_id | Volume ID for data volume for instance, if applicable | `string` | n/a | yes |
| envname | Environment (group) name for cluster | `string` | n/a | yes |
| fail2ban\_ignore\_ip | Non-localhost ip addresses or CIDRs that fail2ban should ignore | `string` | `""` | no |
| id\_base | cloud-init instance-id base; empty string ("") means recreate os volume, cloudinit, and domain on any userdata change, otherwise a change in this id\_base will result in that full change | `string` | `""` | no |
| internal\_domain | Cluster's internal network domain name | `string` | n/a | yes |
| network\_id | ID of network for attaching the network interface | `string` | n/a | yes |
| ntp\_servers | If specified VM will use these servers for NTP instead of the default. Forces use of chrony | `list(string)` | `null` | no |
| os\_base\_volume\_id | volume-id of Libvirt image to use as backing store of OS disk image | `string` | n/a | yes |
| os\_volume\_pool | Libvirt storage pool for the OS disk image | `string` | `null` | no |
| os\_volume\_size | Size of OS disk image in bytes | `string` | `null` | no |
| userdata\_apt | Apt configuration | <pre>object({<br>		add_apt_repo_match = optional(string)<br>		conf = optional(string)<br>		debconf_selections = optional(map(string))<br>		disable_suites = optional(list(string))<br>		ftp_proxy = optional(string)<br>		http_proxy = optional(string)<br>		https_proxy = optional(string)<br>		preserve_sources_list = optional(bool)<br>		primary = optional(list(object({<br>			arches = list(string)<br>			uri = optional(list(string))<br>			search = optional(list(string))<br>			search_dns = optional(bool)<br>		})))<br>		proxy = optional(string)<br>		security = optional(list(object({<br>			arches = list(string)<br>			uri = optional(list(string))<br>			search = optional(list(string))<br>			search_dns = optional(bool)<br>		})))<br>		sources = optional(map(object({<br>			key = optional(string)<br>			keyid = optional(string)<br>			keyserver = optional(string)<br>			source = string<br>		})))<br>		sources_list = optional(string)<br>	})</pre> | `null` | no |
| userdata\_bootcmds | Additional shell commands to run at the start of 'cloud-init' (certain prerequisite commands are always run, for this repo) | `list(list(string))` | `null` | no |
| userdata\_disk\_setups | Map of disks to partitioning schemes | `map(list(number))` | `null` | no |
| userdata\_fs\_setups | Map of disks to list of partitions to format as ext4 if not already formatted | `map(list(number))` | `null` | no |
| userdata\_groups | List of additional groups to ensure exist on system | `list(string)` | `null` | no |
| userdata\_mounts | List of fstab entries to add to system | `list(list(string))` | `null` | no |
| userdata\_packages | List of packages to install via userdata packages config | `list(string)` | `null` | no |
| userdata\_runcmds | Additional shell commands to run at the end of 'cloud-init' (certain prerequisite commands are always run, for this repo) | `list(list(string))` | `null` | no |
| userdata\_ssh\_genkeytypes | SSH host key types to generate | `list(string)` | `null` | no |
| userdata\_ssh\_keys | Specify SSH host keys to use (no new keys will be generated) | <pre>object({<br>		ecdsa_private = string<br>		ecdsa_public = string<br>		ed25519_private = string<br>		ed25519_public = string<br>	})</pre> | `null` | no |
| userdata\_ufwrules | List of additional firewall (UFW) rules | <pre>list(object({<br>		from = string<br>		interface = string<br>		port = number<br>		proto = optional(string)<br>		to = string<br>	}))</pre> | `null` | no |
| userdata\_users | Additional users to add via cloud-init | <pre>list(object({<br>		name = string<br>		gecos = optional(string)<br>		groups = optional(list(string))<br>		homedir = optional(string)<br>		lock_passwd = optional(bool)<br>		shell = optional(string)<br>		ssh_authorized_keys = optional(list(string))<br>		system = optional(bool)<br>	}))</pre> | `null` | no |
| userdata\_write\_files | Additional files to add via cloud-init | <pre>list(object({<br>		append = optional(bool)<br>		encoding = optional(string)<br>		file_source_path = string<br>		owner = optional(string)<br>		path = string<br>		permissions = optional(string)<br>	}))</pre> | `null` | no |
| userdata\_write\_templates | Additional template files to write via cloud-init | <pre>list(object({<br>		append = optional(bool)<br>		encoding = optional(string)<br>		owner = optional(string)<br>		path = string<br>		permissions = optional(string)<br>		template_source_path = string<br>		tvars = any<br>	}))</pre> | `null` | no |
| vm\_memory | Max RAM for the instance | `number` | `null` | no |
| vm\_name | Hostname and Libvirt domain name for the instance (just one name) | `string` | n/a | yes |
| vm\_vcpu | Number of virtual CPU for the instance | `number` | `null` | no |

### Outputs

| Name | Description |
|------|-------------|
| instance\_ip\_primary | Primary IP of instances |

[//]: # (END_TF_DOCS)
