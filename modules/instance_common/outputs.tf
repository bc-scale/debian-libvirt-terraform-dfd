output "instance_ip_primary" {
	description = "Primary IP of instances"
	value = libvirt_domain.instance.network_interface[0].addresses[0]
}
