terraform {
	required_providers {
		libvirt = {
			source = "dmacvicar/libvirt"
		}
		random = {
			source = "hashicorp/random"
		}
	}
}

resource "random_id" "instance_metadata" {
	byte_length = 32

	keepers = {
		os_base_volume_id = "${var.os_base_volume_id}"
		hostname = local.cloudinit_metadata_keeper.hostname
		# For deployment set id_base to non-empty
		# Otherwise any change in user_data will be treated as a new instance
		userdata_change_id = local.cloudinit_metadata_keeper.userdata_change_id
	}

	lifecycle {
		create_before_destroy = true
	}
}

resource "random_id" "instance_cloudinit" {
	byte_length = 16

	keepers = {
		cloudinit_volume_name = "${var.vm_name}_cloudinit.iso"
		cloudinit_metadata_id = "${random_id.instance_metadata.hex}"
		cloudinit_userdata = local.cloudinit_cloud_config
	}

	lifecycle {
		create_before_destroy = true
	}
}

resource "random_id" "instance_os_volume" {
	byte_length = 16

	keepers = {
		base_volume_id = "${var.os_base_volume_id}"
		os_volume_prefix = "${var.vm_name}_os.qcow2"
		userdata_change_id = local.cloudinit_metadata_keeper.userdata_change_id
	}

	lifecycle {
		create_before_destroy = true
	}
}

resource "libvirt_cloudinit_disk" "instance_cloudinit" {
	name = "${var.vm_name}_cloudinit_${random_id.instance_cloudinit.hex}.iso"
	pool = var.cloudinit_volume_pool
	meta_data = jsonencode({
		hostname = local.cloudinit_metadata_keeper.hostname
		instance-id = "${random_id.instance_metadata.hex}"
	})
	user_data = local.cloudinit_cloud_config

	lifecycle {
		create_before_destroy = true
	}
}

resource "libvirt_volume" "instance_os_volume" {
	base_volume_id = var.os_base_volume_id
	format = "qcow2"
	name = "${var.vm_name}_os_${random_id.instance_os_volume.hex}.qcow2"
	pool = var.os_volume_pool
	size = var.os_volume_size
}

resource "libvirt_domain" "instance" {
	autostart = true
	memory = var.vm_memory
	name = var.vm_name
	qemu_agent = true
	running = true
	vcpu = var.vm_vcpu

	cloudinit = libvirt_cloudinit_disk.instance_cloudinit.id

	console {
		type = "pty"
		target_port = "0"
	}

	dynamic "disk" {
		for_each = compact(concat([libvirt_volume.instance_os_volume.id], [var.data_volume_id]))

		content {
			volume_id = disk.value
		}
	}

	network_interface {
		network_id = var.network_id
		wait_for_lease = true
	}

	video {
		type = "virtio"
	}

	# Verify cloud-init completed successfully before declaring success
	provisioner "remote-exec" {
		connection {
			agent = true
			bastion_host = var.conn_bastion_host
			bastion_port = var.conn_bastion_port
			bastion_user = var.conn_bastion_user
			host = libvirt_domain.instance.network_interface.0.addresses.0
			port = var.conn_port
			timeout = var.conn_timeout
			type = "ssh"
			user = var.conn_user
		}

		inline = [
			"if ! cloud-init status --wait --long; then exit 255; fi"
		]
	}

	lifecycle {
		create_before_destroy = false
		replace_triggered_by = [
			libvirt_cloudinit_disk.instance_cloudinit.id,
			libvirt_volume.instance_os_volume.id
		]
	}
}
