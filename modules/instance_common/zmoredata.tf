locals {
	cloudinit_metadata_keeper = {
		hostname = "${var.vm_name}"
		# For deployment set userdata.id_base to non-empty
		# Otherwise any change in user_data will be treated as a new instance
		userdata_change_id = coalesce(var.id_base, local.cloudinit_cloud_config)
	}
}
