locals {
	cloudinit_cloud_config = templatefile("${path.module}/templates/cloud_config.tftpl.yaml", {
		apt = var.userdata_apt
		bootcmd = yamlencode({
			"bootcmd" = concat([ for l in [
				[
					["cloud-init-per", "once", "configAptEarly", "sh", "-c", "if [ -n \"${var.apt_http_proxy}\" ]; then echo 'Acquire::http::Proxy \"${var.apt_http_proxy}\";' >/etc/apt/apt.conf.d/100early-aptproxy; fi"],
					["cloud-init-per", "once", "delayclusterboots", "bash", "-c", "sleep $((RANDOM / 2000))"],
					["cloud-init-per", "once", "updateAptEarly", "apt-get", "update"],
					["cloud-init-per", "once", "installQemuAgentEarly", "sh", "-c", "DEBIAN_FRONTEND=noninteractive apt-get install -y qemu-guest-agent"],
					["cloud-init-per", "once", "ensureStartQemuAgentEarly", "systemctl", "start", "--no-block", "qemu-guest-agent"]
				],
				var.userdata_bootcmds
			] : l if l != null]...)
		})
		disk_setups = var.userdata_disk_setups
		fs_setups = var.userdata_fs_setups
		fqdn = "${var.vm_name}.${var.internal_domain}"
		groups = compact(flatten(
			[
				"adm",
				"sudo",
				"sudonp",
				try([for v in var.userdata_groups : v], "")
			]
		))
		hostname = "${var.vm_name}"
		mounts = var.userdata_mounts
		ntp_servers = var.ntp_servers
		packages = yamlencode({
			"packages" = compact(flatten(
				[
					"molly-guard", # Prevent shooting self in foot
					"powermgmt-base", # To reduce unattended-upgrades syslog complaints
					"python3-gi", # To reduce unattended-upgrades syslog complaints
					"ufw", # b/c firewalld has problematic dbus dependency
					"unattended-upgrades", # auto-update packages
					try([for v in var.userdata_packages : v], "")
				]
			))
		})
		package_actions = yamlencode({
			package_update = true
			package_upgrade = true
			package_reboot_if_required = true
		})
		preserve_hostname = false
		runcmd = yamlencode({
			"runcmd" = concat([for l in [
				[
					["set", "-xe"],
					["systemctl", "reload", "apparmor"]
				],
				var.userdata_runcmds,
				[
					["bash", "/run/ci-files/conf-ufw"],
					["systemctl", "daemon-reload"],
					["rm", "-rf", "/run/ci-files"]
				]
			] : l if l != null]...)
		})
		ssh_genkeytypes = var.userdata_ssh_genkeytypes
		ssh_keys = var.userdata_ssh_keys
		users = concat([ for l in [
			[
				{
					homedir = "${var.admin_base_homedir}/${var.admin_username}"
					gecos = var.admin_gecos
					groups = ["adm","sudo","sudonp"]
					lock_passwd = true
					name = var.admin_username
					shell = "/bin/bash"
					ssh_authorized_keys = [
						"no-agent-forwarding,no-port-forwarding ${var.admin_sshkey}"
					]
				}
			],
			var.userdata_users
		] : l if l != null]...)
		write_files = concat([ for l in [
			[
				{
					content = base64encode("%sudonp ALL=(ALL:ALL) NOPASSWD: ALL")
					encoding = "b64"
					path = "/etc/sudoers.d/sudonp"
					permissions = "0600"
				},
				{
					content = base64encode(file("${path.module}/files/common/50unattended-upgrades"))
					encoding = "b64"
					path = "/etc/apt/apt.conf.d/50unattended-upgrades"
				},
				{
					content = base64encode(templatefile("${path.module}/templates/conf-ufw.tftpl", {
						sshd_port = var.conn_port
						ufwrules = var.userdata_ufwrules
					}))
					encoding = "b64"
					path = "/run/ci-files/conf-ufw"
					permissions = "0755"
				},
				{
					content = base64encode(templatefile("${path.module}/templates/jail.local.tftpl", {
						remote_ignore_ip = var.fail2ban_ignore_ip
					}))
					encoding = "b64"
					path = "/etc/fail2ban/jail.local"
				},{
					content = base64encode(templatefile("${path.module}/templates/local-home.site.local", {
						admin_base_homedir = var.admin_base_homedir
					}))
					encoding = "b64"
					path = "/etc/apparmor.d/tunables/home.d/local-home.site.local"
				},
				{
					content = base64encode(templatefile("${path.module}/templates/sshd_custom.conf.tftpl", {
						sshd_port = "${var.conn_port}"
					}))
					encoding = "b64"
					path = "/etc/ssh/sshd_config.d/sshd_custom.conf"
				},
				{
					content = base64encode(templatefile("${path.module}/templates/ssh_jail.local.tftpl", {
						sshd_port = var.conn_port
					}))
					encoding = "b64"
					path = "/etc/fail2ban/jail.d/ssh.local"
				}
			],
			try([for wfobj in var.userdata_write_files : {
				append = wfobj.append
				content = base64encode(file(wfobj.file_source_path))
				encoding = wfobj.encoding
				owner = wfobj.owner
				path = wfobj.path
				permissions = wfobj.permissions
			} if wfobj != null], []),
			try([for wtobj in var.userdata_write_templates : {
				append = wtobj.append
				content = base64encode(templatefile(wtobj.template_source_path, {
					tvars = wtobj.tvars
				}))
				encoding = wtobj.encoding
				owner = wtobj.owner
				path = wtobj.path
				permissions = wtobj.permissions
			} if wtobj != null], [])
		] : l if l != null]...)
	})
}
