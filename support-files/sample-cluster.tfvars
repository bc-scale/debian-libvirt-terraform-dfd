# To avoid using excessive time and bandwidth, having a local cache
# or mirror of the package repos is recommended. apt-cacher-ng is
# convenient in that you don't need to download a full mirror, instead
# you are able to reuse the packages downloaded by other instances.
apt_http_proxy = "http://192.168.75.1:3142"
admin_sshkey = ## Add Admin SSH authorized key here ##
envname = "ddev"
envnet_v4cidr = "192.168.75.0/24"
cluster_data_volumes = {
	"gate" = {
		format = "qcow2"
		name = "gatedataddev.qcow2"
		pool = "default"
		size = 2147483648
	}
	"gdav" = {
		format = "qcow2"
		name = "gdavdataddev.qcow2"
		pool = "default"
		size = 21474836480
	}
	"imap" = {
		format = "qcow2"
		name = "imapdataddev.qcow2"
		pool = "default"
		size = 21474836480
	}
	"smtp01" = {
		format = "qcow2"
		name = "smtp01dataddev.qcow2"
		pool = "default"
		size = 10737418240
	}
	"webhost" = {
		format = "qcow2"
		name = "wwwdataddev.qcow2"
		pool = "default"
		size = 10737418240
	}
}
instantiation_data = {
	"airlock" = {
		conn_bastion_host = "vmhost.##external domain"
		conn_bastion_port = 673
		conn_bastion_user = "bastion_user"
		conn_domain = "ddev"
		conn_port = 670
		conn_timeout = "8m"
		conn_user = "admin"
		fail2ban_ignore_ip = "192.168.75.0/24"
		id_base = ""
		os_volume_pool = "default"
		os_volume_size = "8589934592"
		userdata_packages = [
			"fail2ban"
		]
		userdata_runcmds = [
			["systemctl", "restart", "fail2ban"]
		]
		userdata_users = [
			{
				homedir = "/local-home/bastion_user"
				name = "bastion_user"
				ssh_authorized_keys = [
					## Set Bastion user authorized_keys here ##
				]
			}
		]
		vm_memory = 512
		vm_name = "airlockddev"
		vm_vcpu = 1
	}
	"gate" = {
		conn_bastion_host = "vmhost.##external domain"
		conn_bastion_port = 673
		conn_bastion_user = "bastion_user"
		conn_domain = "ddev"
		conn_port = 22
		conn_timeout = "8m"
		conn_user = "admin"
		fail2ban_ignore_ip = "192.168.75.0/24"
		id_base = ""
		os_volume_pool = "default"
		os_volume_size = "8589934592"
		userdata_bootcmds = [
			["mkdir", "-p", "/etc/letsencrypt"]
		]
		userdata_disk_setups = {
			# Let's encrypt partition
			"/dev/vdb" = [100]
		}
		userdata_fs_setups = {
			"/dev/vdb" = [1]
		}
		userdata_mounts = [
			["/dev/vdb1", "/etc/letsencrypt"],
		]
		vm_memory = 2048
		vm_name = "gateddev"
		vm_vcpu = 3
	}
	"gdav" = {
		conn_bastion_host = "vmhost.##external domain"
		conn_bastion_port = 673
		conn_bastion_user = "bastion_user"
		conn_domain = "ddev"
		conn_port = 22
		conn_timeout = "16m"
		conn_user = "admin"
		fail2ban_ignore_ip = "192.168.75.0/24"
		id_base = ""
		os_volume_pool = "default"
		os_volume_size = "8589934592"
		userdata_bootcmds = [
			["mkdir", "-p", "/var/lib/radicale"]
		]
		userdata_disk_setups = {
			"/dev/vdb" = [100]
		}
		userdata_fs_setups = {
			"/dev/vdb" = [1]
		}
		userdata_mounts = [
			["/dev/vdb1", "/var/lib/radicale"]
		]
		vm_memory = 512
		vm_name = "gdavddev"
		vm_vcpu = 1
	}
	"imap" = {
		conn_bastion_host = "vmhost.##external domain"
		conn_bastion_port = 673
		conn_bastion_user = "bastion_user"
		conn_domain = "ddev"
		conn_port = 22
		conn_timeout = "20m"
		conn_user = "admin"
		fail2ban_ignore_ip = "192.168.75.0/24"
		id_base = ""
		os_volume_pool = "default"
		os_volume_size = "8589934592"
		userdata_bootcmds = [
			["mkdir", "-p", "/var/vmail"]
		]
		userdata_disk_setups = {
			"/dev/vdb" = [100]
		}
		userdata_fs_setups = {
			"/dev/vdb" = [1]
		}
		userdata_mounts = [
			["/dev/vdb1", "/var/vmail"]
		]
		vm_memory = 2048
		vm_name = "imapddev"
		vm_vcpu = 2
	}
	"smtp01" = {
		conn_bastion_host = "vmhost.##external domain"
		conn_bastion_port = 673
		conn_bastion_user = "bastion_user"
		conn_domain = "ddev"
		conn_port = 22
		conn_timeout = "20m"
		conn_user = "admin"
		fail2ban_ignore_ip = "192.168.75.0/24"
		id_base = ""
		os_volume_pool = "default"
		os_volume_size = "8589934592"
		userdata_bootcmds = [
			["mkdir", "-p", "/etc/letsencrypt", "/var/spool/postfix"]
		]
		userdata_disk_setups = {
			# Let's encrypt partition and the rest for mail
			"/dev/vdb" = [5, 95]
		}
		userdata_fs_setups = {
			"/dev/vdb" = [1, 2]
		}
		userdata_mounts = [
			["/dev/vdb1", "/etc/letsencrypt"],
			["/dev/vdb2", "/var/spool/postfix"]
		]
		vm_memory = 4096
		vm_name = "smtp01ddev"
		vm_vcpu = 2
	}
	"webhost" = {
		conn_bastion_host = "vmhost.##external domain"
		conn_bastion_port = 673
		conn_bastion_user = "bastion_user"
		conn_domain = "ddev"
		conn_port = 22
		conn_timeout = "8m"
		conn_user = "admin"
		fail2ban_ignore_ip = "192.168.75.0/24"
		id_base = ""
		os_volume_pool = "default"
		os_volume_size = "8589934592"
		userdata_bootcmds = [
			["mkdir", "-p", "/var/www"]
		]
		userdata_disk_setups = {
			"/dev/vdb" = [100]
		}
		userdata_fs_setups = {
			"/dev/vdb" = [1]
		}
		userdata_mounts = [
			["/dev/vdb1", "/var/www"]
		]
		vm_memory = 2048
		vm_name = "wwwddev"
		vm_vcpu = 4
	}
}
internal_domain = "ddev"
libvirt_os_base_pool = "default"
libvirt_os_source_location = "https://cloud.debian.org/images/cloud/bullseye/20221205-1220/debian-11-generic-amd64-20221205-1220.qcow2"
ntp_servers = [
	"192.168.75.1"
]
